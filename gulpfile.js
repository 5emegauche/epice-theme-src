var gulp = require('gulp');
var path = require('path');
var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat');
var less = require('gulp-less');
var uglify = require('gulp-uglify');
var resolveDependencies = require('gulp-resolve-dependencies');

var basePaths = {
  src: './',
  dest: '../wordpress-new/wp-content/themes/epice/'
}

gulp.task('default', ['less','js'], function() {
  gulp.watch(basePaths.src+'less/**/*.less', ['less']);
  gulp.watch(basePaths.src+'js/**/*.js', ['js']);
});

gulp.task('less', function () {
  gulp.src(basePaths.src+'less/core.less')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(concat('style.css'))
    .pipe(gulp.dest(basePaths.dest));

  gulp.src(basePaths.src+'less/admin.less')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(concat('admin.css'))
    .pipe(gulp.dest(basePaths.dest+'css'));

    gulp.src(basePaths.src+'less/ie.less')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(concat('ie.css'))
    .pipe(gulp.dest(basePaths.dest+'css'));
    
/*
    gulp.src(basePaths.src+'less/ie-8.less')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(concat('ie-8.css'))
    .pipe(gulp.dest(basePaths.dest+'css'));

    gulp.src(basePaths.src+'less/ie-9.less')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(concat('ie-9.css'))
    .pipe(gulp.dest(basePaths.dest+'css'));
    */
});

gulp.task('js', function () {
  gulp.src(basePaths.src+'js/core.js')
  .pipe(resolveDependencies({
      pattern: /\* @requires [\s-]*(.*?\.js)/g
    }))
    .pipe(concat('epice-core.js'))
    .pipe(uglify())
    .pipe(gulp.dest(basePaths.dest+'js'));

    gulp.src(basePaths.src+'js/epice-ajax.js')
  /*.pipe(resolveDependencies({
      pattern: /\* @requires [\s-]*(.*?\.js)/g
    }))
    .pipe(concat('epice-ajax.js'))*/
    //.pipe(uglify())
    .pipe(gulp.dest(basePaths.dest+'js'));

    gulp.src(basePaths.src+'js/epice-admin.js')
  /*.pipe(resolveDependencies({
      pattern: /\* @requires [\s-]*(.*?\.js)/g
    }))
    .pipe(concat('epice-admin.js'))*/
    .pipe(uglify())
    .pipe(gulp.dest(basePaths.dest+'js'));
/*
    gulp.src(basePaths.src+'js/core-ie.js')
  .pipe(resolveDependencies({
      pattern: /\* @requires [\s-]*(.*?\.js)/g
    }))
    .pipe(concat('core-ie.js'))
    .pipe(uglify())
    .pipe(gulp.dest(basePaths.dest+'js'));*/
});

