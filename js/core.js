/**
 * @requires lib/modernizr.custom.touch.js
 * @requires lib/jquery.js
 * @requires lib/jquery.isotope.min.js
 * @requires lib/jquery.jcarousel-core.js
 */


var externalLinks = [
  'http://www.epice.com/uk/shop.html'
];

var _epiceConfig = {
  'hiddenBlock': 'hidden-block',
  'fadeBlock': 'fade-block',
  'clickEvent': 'click',
  'overEvent': 'mouseover',
  'outEvent': 'mouseout',
  'mobileWidth': 940
};

if(window.jQuery !== undefined)
{
  var jqNo = jQuery.noConflict();
  
  var _config = {
    'isMobile': false,
    'mobileClass': 'mobile-viewport',
    'mobileMenu': null,
    'mobileMenuNav': null,
    'mobileMenuFooter': null,
    'mobileMenuButton': null,
    'activeClass': 'ui-state-active',
    'noScrollClass': 'no-scroll',
    'documentRoot': jqNo('html'),
    'documentBody': jqNo('body'),
    'mainHeader': jqNo('.page-header'),
    'menuLeftTop': jqNo('.top-nav.left-nav li'),
    'menuRightTop': jqNo('.top-nav.right-nav li'),
    'filtersList': jqNo('#filters-list li'),
    'headerSidebarBlock': jqNo('#primary-sidebar'),
    'insertZone': null,
    'newsletterBlockContent': '',
    'newsletterInit': false
  };

  // Related posts début
  
  var _relatedConfig = {
    'block': jqNo('#related-posts'),
    'list': jqNo('#related-posts-list'),
    'items': jqNo('#related-posts-list > li'),
    'listBlock': null,
    'viewport': null,
    'relatedPostsItemsCount': 3,
    'carouselInstance': null,
    'prev': null,
    'next': null
  };

  function setRelatedCarousel()
  {
    if(_relatedConfig.items.length > _relatedConfig.relatedPostsItemsCount)
    {
      if(_relatedConfig.viewport === null)
      {
        _relatedConfig.list.wrap('<div class="related-posts-viewport"/>');
        _relatedConfig.viewport = jqNo('.related-posts-viewport');
      }

      if(_relatedConfig.listBlock === null)
      {
        _relatedConfig.viewport.wrap('<div class="related-posts-list-block"/>');
        _relatedConfig.listBlock = jqNo('.related-posts-list-block');
      }

      if(_relatedConfig.prev === null)
      {
        _relatedConfig.listBlock.append('<span class="related-posts-prev related-posts-button"/>');
        _relatedConfig.prev = jqNo('.related-posts-prev');
      }
      if(_relatedConfig.next === null)
      {
        _relatedConfig.listBlock.append('<span class="related-posts-next related-posts-button"/>');
        _relatedConfig.next = jqNo('.related-posts-next');
      }

      var viewportWidth = _relatedConfig.viewport.outerWidth();

      var listWidth = 0;
      _relatedConfig.items.each(function()
      {
        var currentItem = jqNo(this);
        var itemWidth = currentItem.outerWidth(true);
        var itemLeftMargin = currentItem.css('margin-left');

        var itemRelativeWidth = ((itemWidth/viewportWidth)*100)+'%';
        var itemRelativeLeftMargin = ((itemLeftMargin/viewportWidth)*100)+'%';

        currentItem.css({'margin-left': itemRelativeLeftMargin,'width': itemRelativeWidth});

        listWidth += itemWidth;

        //console.log('liste :: '+listWidth+' // item :: '+currentItem.width()+' :: '+itemWidth);
      });
      
      // problème taille ie 9 arrondi (+1)
      _relatedConfig.list.css({width: ((listWidth/viewportWidth)*100+1)+'%'});
      
      _relatedConfig.carouselInstance = _relatedConfig.block.jcarousel({
        list: '#related-posts-list',
        wrap: 'circular'
      });

      jqNo('.related-posts-prev').on(_epiceConfig.clickEvent,function()
      {
        _relatedConfig.block.jcarousel('scroll', '-=1');
      });

      jqNo('.related-posts-next').on(_epiceConfig.clickEvent,function()
      {
        _relatedConfig.block.jcarousel('scroll', '+=1');
      });
    }
  }

  function unsetRelatedCarousel()
  {
    if(_relatedConfig.carouselInstance !== null)
    {
      _relatedConfig.block.jcarousel('destroy');
      _relatedConfig.carouselInstance = null;
    }

    _relatedConfig.list.css({width: 'auto',left: 0});
  }

  function configureNewsletterBlock(nBlock)
  {
    var _newsletterConfig = {
      'block': nBlock.find('.widget_wysija_cont'),
      'form': nBlock.find('.widget_wysija_cont form'),
      'submit': nBlock.find('.wysija-submit.wysija-submit-field'),
      'messageBlock': nBlock.find('.wysija-msg'),
      'animDuration': 500
    }

    if(!_config.isMobile)
    {
      _newsletterConfig.block.hide();

      nBlock.find('.widget-title').on(_epiceConfig.clickEvent,function()
      {
        var titleBlock = jqNo(this);

        if(titleBlock.hasClass(_config.activeClass))
        {
          _newsletterConfig.block.fadeOut(_newsletterConfig.animDuration);
        }
        else
        {
          _newsletterConfig.block.fadeIn(_newsletterConfig.animDuration);
        }
        
        titleBlock.toggleClass(_config.activeClass);
      });
    }
      
    /*if(!_config.newsletterInit)
    {*/
      // Formulaire abonnement newsletter début
      
      function hideMessages()
      {
        _newsletterConfig.messageBlock.fadeOut(_newsletterConfig.animDuration*2);
        _newsletterConfig.form.fadeIn(_newsletterConfig.animDuration*2);
      }

      function hideErrors()
      {
        var error = jqNo('.formError',_newsletterConfig.block);
        if(error.length > 0)
        {
          error.fadeOut(_newsletterConfig.animDuration,function()
          {
            error.remove();
          });
        }
      }
      
      _newsletterConfig.submit.on(_epiceConfig.clickEvent,function()
      {
        _newsletterConfig.messageBlock.html('').show();
        window.setTimeout(hideMessages,_newsletterConfig.animDuration*8);
        window.setTimeout(hideErrors,_newsletterConfig.animDuration*3);
      });
    //}
    
    _config.newsletterInit = true;
    // Formulaire abonnement newsletter fin
  }

  // Gestion du viewport Desktop
  
  function desktopViewport()
  {
    setRelatedCarousel();
    _config.documentBody.removeClass(_config.mobileClass);
    _config.documentBody.css({'padding-top': 0});

    if(_config.insertZone !== null)
    {
        //_config.insertZone.html('');
    }
    if(_config.newsletterBlockContent != '')
    {
      _config.headerSidebarBlock.html(_config.newsletterBlockContent);
    }
    configureNewsletterBlock(_config.headerSidebarBlock.find('div'),false);
  }

  function toggleMenu()
  {
    var winHeight = jqNo(window).height();
    //_config.mobileMenu.outerHeight(winHeight);
    _config.mobileMenu.toggleClass(_config.activeClass);
    _config.documentRoot.toggleClass(_config.noScrollClass);
  }

  // Gestion du viewport Mobile

  function mobileViewport()
  {
    unsetRelatedCarousel();
    _config.documentBody.addClass(_config.mobileClass);

    if(_config.mobileMenu === null)
    {
      var menuCode = '<div id="mobile-menu">';
      
      menuCode += '<nav class="mobile-element" id="mobile-nav"><ul>';
      
      _config.filtersList.each(function()
      {
        menuCode += '<li>'+jqNo(this).html()+'</li>';
      });

      _config.menuLeftTop.each(function()
      {
        menuCode += '<li>'+jqNo(this).html()+'</li>';
      });

       _config.menuRightTop.each(function()
      {
        menuCode += '<li>'+jqNo(this).html()+'</li>';
      });

      menuCode += '</ul><div id="insert-zone" class="newsletter-block"><a href="/newsletter/">Newsletter</a></div></nav>';

      menuCode += '<footer id="mobile-footer"><div class="menu-part"><ul class="social-nav">'+jqNo('#social-nav-list').html()+'</ul></div>';

      menuCode += '<div class="lang-switcher menu-part">'+jqNo('#lang-switcher').html()+'</div></footer>';

      menuCode += '</div>';

      _config.documentBody.append(menuCode);
      _config.mobileMenu = jqNo('#mobile-menu');
      
      _config.mobileMenuNav = jqNo('#mobile-nav');
      
      _config.mobileMenuFooter = jqNo('#mobile-footer');

      _config.insertZone = jqNo('#insert-zone');
    }
    
    if(_config.newsletterBlockContent != '')
    {
      /*_config.insertZone.html(_config.newsletterBlockContent);
      _config.headerSidebarBlock.html('');
      configureNewsletterBlock(_config.insertZone.find('div'),true);*/

      //var nsIframe = '<iframe width="100%" scrolling="no" frameborder="0" src="/?wysija-page=1&controller=subscribers&action=wysija_outter&wysija_form=1&external_site=1&wysijap=subscriptions" class="iframe-wysija" vspace="0" tabindex="0" style="position: static; top: 0pt; margin: 0px; border-style: none; height: 330px; left: 0pt; visibility: visible;" marginwidth="0" marginheight="0" hspace="0" allowtransparency="true" title="Abonnement via MailPoet"></iframe>';
      //_config.insertZone.html(nsIframe);
    }

    if(_config.mobileMenuButton === null)
    {
      var button = jqNo('<span class="mobile-menu-button mobile-element"/>').on(_epiceConfig.clickEvent,function()
      {
        //jqNo(this).toggleClass(_config.activeClass);
        toggleMenu();
      });

      _config.mainHeader.append(button);
      _config.mobileMenuButton = jqNo('.mobile-menu-button');
    }

    var menuHead = jqNo('.page-header',_config.mobileMenu);
    if(menuHead.length == 0)
    {
      _config.mobileMenu.prepend(_config.mainHeader.clone(true));
    }
    menuHead = jqNo('.page-header',_config.mobileMenu);

    jqNo('.mobile-menu-button',_config.mobileMenu).addClass(_config.activeClass);

    var headerHeight = menuHead.outerHeight();

    var topPosition = headerHeight;

    _config.documentBody.css({'padding-top': topPosition+20});
    //_config.mobileMenu.css({'padding-top': topPosition});

    var footerHeight = _config.mobileMenuFooter.outerHeight();

    _config.mobileMenu.css({'padding-bottom': footerHeight});
//
    _config.mobileMenuNav.css({'top': topPosition,'bottom': footerHeight});
  }

  function resizeWindow()
  {
    var wW = jqNo(this).width();

    if(wW <= _epiceConfig.mobileWidth)
    {
      _config.isMobile = true;
    }
    else
    {
      _config.isMobile = false;
    }

    if(_config.isMobile)
    {
      mobileViewport();
    }
    else
    {
      desktopViewport();
    }
  }

  function verticalCenter()
  {
    jqNo('.vertical-middle').each(function()
    {
      var currentItem = jqNo(this);

      var blockHeight = currentItem.outerHeight();

      var detailBlock = currentItem.find('.block-detail');

      if(detailBlock.length == 0)
      {
        currentItem.wrapInner('<div class="block-detail"/>');

        detailBlock = currentItem.find('.block-detail');
      }

      var detailHeight = detailBlock.outerHeight();
      var topDelta = (blockHeight-detailHeight)/2;

      detailBlock.css({'padding-top': topDelta});
    });
  }

  function displayPictureBlock()
  {
    jqNo('.picture-block-item').each(function()
    {
      var currentItem = jqNo(this);
      var url;

      url = currentItem.find('a.block-link').attr('href');

      var codeItem = '<div class="picture-effect"/>';
      
      if(url !== undefined)
      {
        codeItem += '<a href="'+url+'" class="cover-block"/>';
      }

      currentItem.append(codeItem);

      var content = currentItem.find('.block-content');
      
      var blockHeight = currentItem.height();
      var blockWidth = currentItem.width();

      var detailBlock = content.find('.block-detail');

      if(detailBlock.length == 0)
      {
        content.wrapInner('<div class="block-detail"/>');

        detailBlock = content.find('.block-detail');
      }

      var detailHeight = detailBlock.height();
      var detailWidth = detailBlock.width();
      var top = Math.round((blockHeight-detailHeight)/2);
      //var left = Math.round(detailWidth/2);
      //console.log(blockHeight+' :: '+detailHeight);
      
      //detailBlock.css({'padding-top': top});
      
      currentItem.find('.cover-block').on(_epiceConfig.overEvent,function()
        {
          currentItem.addClass(_epiceConfig.hiddenBlock);
        }).on(_epiceConfig.outEvent,function()
        {
          currentItem.removeClass(_epiceConfig.hiddenBlock);
        });

      currentItem.addClass(_config.activeClass);
    });
  }

  jqNo(document).on('touchmove',function(ev)
  {
    if(_config.documentRoot.hasClass(_config.noScrollClass))
    {
      //ev.preventDefault();
    }
  });

  // Related posts fin

  jqNo(window).on('resize',resizeWindow);
  
  jqNo(window).on('load',function()
  {
    displayPictureBlock();

    // Contenu centré verticalement

    verticalCenter();
/*
    var isotopeContainer = jqNo('.post-list');
    if(isotopeContainer.length > 0)
    {
      isotopeContainer.isotope({
        itemSelector: '.post',
        layoutMode: 'masonry',
        masonry: {
          columnWidth: '.post',
          gutter: 30
        }
      });
    }

    jqNo('body.home .filters-list li').on(_epiceConfig.clickEvent,function(ev)
    {
      var currentItem = jqNo(this);
      var prefix = '.filter-';
      
      var filter = currentItem.data('filter');

      var selector = '';

      if(filter != '')
      {
        selector = prefix+filter;
      }
      
      if(selector != '')
      {
        isotopeContainer.isotope({'filter': selector});
      }
      
      ev.preventDefault();
    });
    *
    * jqNo('body.home header .home').css({'cursor': 'pointer'}).on(_epiceConfig.clickEvent,function(ev)
    {
      isotopeContainer.isotope({'filter': '.post'});
    });
*/
    
  });

  jqNo(function()
  {
    jqNo('.header-bottom a').each(function()
    {
      var currentLink = jqNo(this);
//alert(jQuery.inArray(currentLink.attr('href'),externalLinks))
      if(jQuery.inArray(currentLink.attr('href'),externalLinks) !== -1)
      {
        currentLink.attr('target','_blank');
      }
    });
    
    _config.newsletterBlockContent = _config.headerSidebarBlock.html();

    resizeWindow();
    
    // Roll-over sur les pictos sociaux
    var imagesDir = 'images/';
    if(pageConfig.themeUrl !== undefined)
    {
      imagesDir = pageConfig.themeUrl+'/'+imagesDir;
    }
    jqNo('.ssba a img').each(function()
    {
      var currentImg = jqNo(this);
      var ident = currentImg.parent().attr('class');
      var identPattern = /ssba_([a-zA-Z0-9]+)_share/;

      var key = null;
      var idParts = ident.match(identPattern);
      if(idParts.length >= 2)
      {
        key = idParts[1];
      }
      
      currentImg.data('default',currentImg.attr('src'));
      currentImg.data('on',imagesDir+''+key+'-on.png');

      currentImg.on('mouseover',function()
      {
        jqNo(this).attr('src',jqNo(this).data('on'));
      }).on('mouseout',function()
      {
        jqNo(this).attr('src',jqNo(this).data('default'));
      });
      
    });

    
  });
}
