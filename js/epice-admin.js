if(window.jQuery !== undefined)
{
  jQuery(function()
  {
    jQuery('div.media-box').each(function()
    {
      var bloc = jQuery(this);
      var preview = bloc.find('p.epice-preview');
      
      bloc.find('.removeImageBtn').click(function(ev)
      {
        preview.html('');   
        bloc.find('input.metaValueField').val('');
        ev.preventDefault();
      });

      bloc.find('.image_upload_button').click(function(ev)
      {
        inputField = bloc.find('.metaValueField');
        tb_show('','media-upload.php?TB_iframe=true');
        window.send_to_editor = function(html)
        {
          url = jQuery(html).attr('href');
          inputField.val(url);
          preview.html('<a target="_blank" href="'+url+'"><img src="'+url+'" alt="" /></a>');  
          tb_remove();
        };
        ev.preventDefault();
      });
    });
  });
}
